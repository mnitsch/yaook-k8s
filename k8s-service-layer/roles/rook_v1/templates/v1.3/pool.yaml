# https://github.com/rook/rook/releases/tag/v1.3.11
# $YOUR_ROOK_REPO/cluster/examples/kubernetes/ceph/

#################################################################################################################
# Create a Ceph pool with settings for replication in production environments. A minimum of 3 OSDs on
# different hosts are required in this example.
#  kubectl create -f pool.yaml
#################################################################################################################

apiVersion: ceph.rook.io/v1
kind: CephBlockPool
metadata:
  name: "{{ item.name }}"
  namespace: "{{ rook_namespace }}"
spec:
  # The failure domain will spread the replicas of the data across different failure zones
  failureDomain: {{ item.failure_domain | default('host') }}
{% if item.erasure_coded | default(False) %}
  erasureCoded:
    dataChunks: {{ item.erasure_coded.data_chunks | default(2) }}
    codingChunks: {{ item.erasure_coded.coding_chunks | default(1) }}
{% else %}
  # For a pool based on raw copies, specify the number of copies. A size of 1 indicates no redundancy.
  replicated:
    size: {{ item.replicated | default(1) }}
    # Disallow setting pool with replica 1, this could lead to data loss without recovery.
    # Make sure you're *ABSOLUTELY CERTAIN* that is what you want
    requireSafeReplicaSize: {{ ((item.replicated | default(1) | int) != 1) | to_json }}
{% endif %}
  deviceClass: {{ item.device_class | default("hdd") | to_json }}
  # Ceph CRUSH root location of the rule
  # For reference: https://docs.ceph.com/docs/nautilus/rados/operations/crush-map/#types-and-buckets
  #crushRoot: my-root
  # The Ceph CRUSH device class associated with the CRUSH replicated rule
  # For reference: https://docs.ceph.com/docs/nautilus/rados/operations/crush-map/#device-classes
  #deviceClass: my-class
  # Set any property on a given pool
  # see https://docs.ceph.com/docs/master/rados/operations/pools/#set-pool-values
  compressionMode: "none"
  parameters:
    # Inline compression mode for the data pool
    # Further reference: https://docs.ceph.com/docs/nautilus/rados/configuration/bluestore-config-ref/#inline-compression
    compressionMode: "none"
    # gives a hint (%) to Ceph in terms of expected consumption of the total cluster capacity of a given pool
    # for more info: https://docs.ceph.com/docs/master/rados/operations/placement-groups/#specifying-expected-pool-size
    #target_size_ratio: ".5"
  # A key/value list of annotations
  # annotations:
  #  key: value
