---
- name: Prepare monitoring stack migration
  # Note: we're deleting the manifests in reverse so that resources are removed before their CRDs
  when: monitoring_migrate_from_v1 | default(False)
  block:
    - name: Warn the user
      debug:
        msg: |
          Please be aware that the monitoring migration is potentially a disruptive task.
          This role removes any k8s resource whose manifest was created out of the jsonnet
          templates. As a consequence you have to have these manifests before you perform
          the migration, e.g., by running mon_v1 once. Other implications are
          * monitoring resources that were not created from jsonnet templates will not be destroyed, including the monitoring namespace and customer specific configmaps and dashboards
          * if executed twice, this role will happily delete any CRDs from v2 that overlap
          * if the migration is cancelled before it's been completed you will be in a world of pain and you will probably have to deal with admission- and mutating web hooks.
          I have decided against additional safety measures (do the manifests exist? is the operator already running?) because they would be too complex. Just be aware of what you're doing.
    - name: Delete old jsonnet manifests (managed-layer)
      k8s:
        state: absent
        apply: yes
        definition: "{{ lookup('file', monitoring_jsonnet_dir_monitoring + '/' + item) }}"
        validate:
          fail_on_error: yes
          strict: yes
      loop: "{{ lookup('file', monitoring_jsonnet_dir_monitoring + '/manifests.files.json', errors='strict' if (k8s_monitoring_enabled and monitoring_use_jsonnet_setup) else 'warn') | default(' [] ', true) | from_json | sort(reverse=true) }}" # noqa 204

    - name: Delete old jsonnet manifests (service-layer)
      k8s:
        state: absent
        apply: yes
        definition: "{{ lookup('file', monitoring_jsonnet_dir_monitoring + '/' + item) }}"
        validate:
          fail_on_error: yes
          strict: yes
      loop: "{{ lookup('file', monitoring_jsonnet_dir_monitoring + '/service-layer.files.json', errors='strict' if (k8s_monitoring_enabled and monitoring_use_jsonnet_setup) else 'warn') | default(' [] ', true) | from_json | sort(reverse=true) }}" # noqa 204
