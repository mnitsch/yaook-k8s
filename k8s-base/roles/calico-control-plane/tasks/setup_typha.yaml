# --------------------------------------------------- #
# The following part checks the Typha CA certificate. #
# --------------------------------------------------- #

- name: Check local and remote presence of Typha CA certificate
  block:
  - name: Check if Typha CA private key is locally present
    delegate_to: localhost
    stat:
      path: "{{ typhaca_key_path }}"
    register: typhaca_key_local

  - name: Check if Typha CA signed certificate is locally present
    delegate_to: localhost
    stat:
      path: "{{ typhaca_crt_path }}"
    register: typhaca_crt_local

  - name: Fail if certificate but private key does not exist for the Typha CA
    fail:
      msg: "### WARNING ###\nThe certificate of the Typha CA is locally present, but not the private key!"
    when: typhaca_crt_local.stat.exists and not typhaca_key_local.stat.exists

  - name: Fetch Typha CA ConfigMap from k8s cluster
    become: yes
    environment:
      KUBECONFIG: /etc/kubernetes/admin.conf
    k8s_info:
      api_version: v1
      kind: ConfigMap
      name: calico-typha-ca
      namespace: kube-system
    register: typhaca_crt_configmap

- name: Validate local Typha CA crt on expiration
  when: typhaca_crt_local.stat.exists
  block:
  - name: Collect Expiration information of the Typha CA crt
    delegate_to: localhost
    openssl_certificate_info:
      path: "{{ typhaca_crt_path }}"
      valid_at:
        point_1: "+104w"
        point_2: "+52w"
    register: typhaca_crt_valid

  # ToDo send a notification
  - name: Warn if the Typha CA crt expires in less than 2 years from now
    debug:
      msg: "### WARNING ###\nThe Typha CA certificate expires in less than 2 years from now"
    when: not typhaca_crt_valid.valid_at.point_1

  - name: Warn if the Typha CA crt expires in less than 1 year from now
    debug:
      msg: "### ERROR ####\nThe Typha CA certificate expires in less than 1 year from now.\nGenerating a new CA certificate now."
    when: not typhaca_crt_valid.valid_at.point_2

- name: Validate local Typha CA crt against the remote one (ConfigMap)
  when: typhaca_crt_local.stat.exists and typhaca_crt_configmap.resources
  block:
  - name: Check that local Typha CA equals the one in the ConfigMap
    vars:
     typha_ca_local_content: "{{ lookup('file', '{{ etc_dir }}/calico/typhaca.crt') | trim | to_json }}"
     typha_ca_configmap_content: "{{ typhaca_crt_configmap.resources[0].data['typhaca.crt'] | trim | to_json }}"
    debug:
      msg: "The local Typha CA and the one remote one (ConfigMap) do equal!"
    when: typha_ca_configmap_content == typha_ca_local_content
    register: typhaca_crt_local_remote_equality

  - name: Fail if the local Typha CA and the one in the ConfigMap do not match
    fail:
      msg: "The local Typha CA and the one in the ConfigMap do not equal!\nSomething is weird."
    when: not typhaca_crt_local_remote_equality

# Generate and upload a new certificate if
# - first run (does not exist)
# - certificate expires soon
- name: Generate Typha CA certificate and create ConfigMap on k8s
  block:
  # We will use mutually authenticated TLS to ensure that calico/node and Typha communicate securely
  # We generate a certificate authority (CA), put it in a ConfigMap, and use it to sign the certificate
  # of Typha and of calico/node.
  # ---
  # The Typha CA private key is regenerated, if the Typha CA certificate is short to expire. This will
  # also lead to the recreation of the Typha CA certificate. In the following tasks, Typha and calico/node
  # will then also regenerate their certificates.
  - name: Regenerate (force) private key of Typha CA certificate if it is short to expire
    when: not (typhaca_crt_valid.valid_at.point_2  | default('false'))
    delegate_to: localhost
    openssl_privatekey:
      path: "{{ typhaca_key_path }}"
      mode: 0600
      size: 4096
      force: yes

  # TODO: we should protect this with a password and store it in a more secure place
  - name: Generate private key for Typha CA certificate
    delegate_to: localhost
    openssl_privatekey:
      path: "{{ typhaca_key_path }}"
      mode: 0600
      size: 4096

  - name: Generate Certificate Signing Request for Typha CA certificate
    delegate_to: localhost
    openssl_csr:
      path: "{{ typhaca_csr_path }}"
      privatekey_path: "{{ typhaca_key_path }}"
      common_name: "Calico Typha CA"
      subject:
        CN: "Calico Typha CA"
      basic_constraints:
        - CA:TRUE
      mode: 0664

  - name: Sign the Certificate Signing Request of Typha CA
    delegate_to: localhost
    openssl_certificate:
      path: "{{ typhaca_crt_path }}"
      privatekey_path: "{{ typhaca_key_path }}"
      csr_path: "{{ typhaca_csr_path }}"
      provider: selfsigned
      mode: 0664
      entrust_not_after: "+3653d"

  # The ConfigMap should always be up-to-date
  - name: Apply local Typha CA certificate to ConfigMap
    become: yes
    environment:
      KUBECONFIG: /etc/kubernetes/admin.conf
    k8s:
      apply: yes
      definition:
        apiVersion: v1
        data:
          typhaca.crt: |
            {{ lookup('file', typhaca_crt_path) }}
        kind: ConfigMap
        metadata:
          name: calico-typha-ca
          namespace: kube-system
      validate:
        fail_on_error: yes
        strict: yes

# ------------------------------------------------ #
# The following part checks the Typha certificate. #
# ------------------------------------------------ #

- name: Check local and remote presence of Typha certificate
  block:
  - name: Check if Typha private key is locally present
    delegate_to: localhost
    stat:
      path: "{{ typha_key_path }}"
    register: typha_key_local

  - name: Check if Typha signed certificate is locally present
    delegate_to: localhost
    stat:
      path: "{{ typha_crt_path }}"
    register: typha_crt_local

  - name: Fail if certificate but private key does not exist for Typha
    fail:
      msg: "### WARNING ###\nThe certificate of Typha is locally present, but not the private key!"
    when: typha_crt_local.stat.exists and not typha_key_local.stat.exists

  - name: Fetch Typha Secret from k8s cluster
    become: yes
    environment:
      KUBECONFIG: /etc/kubernetes/admin.conf
    k8s_info:
      api_version: v1
      kind: Secret
      name: calico-typha-certs
      namespace: kube-system
    register: typha_crt_secret

- name: Validate local Typha crt on expiration
  when: typha_crt_local.stat.exists
  block:
  - name: Collect expiration information of the Typha crt
    delegate_to: localhost
    openssl_certificate_info:
      path: "{{ typha_crt_path }}"
      valid_at:
        point_1: "+35w"
        point_2: "+26w"
    register: typha_crt_valid

  # ToDo send a notification
  - name: Warn if the Typha crt expires in less than 8 months from now
    debug:
      msg: "### WARNING ###\nThe Typha certificate expires in less than 8 months from now"
    when: not typha_crt_valid.valid_at.point_1

  - name: Warn if the Typha crt expires in less than 6 months from now
    debug:
      msg: "### WARNING ###\nThe Typha certificate expires in less than 6 months from now. It will be regenerated."
    when: not typha_crt_valid.valid_at.point_2

- name: Validate local Typha crt against the remote (Secret)
  when: typha_crt_local.stat.exists and typha_crt_secret.resources
  block:
  - name: Check that local Typha crt equals the one stored in the Secret
    vars:
      typha_crt_local_content: "{{ lookup('file', '{{ etc_dir }}/calico/typha.crt') | trim | to_json }}"
      typha_crt_remote_content: "{{ typha_crt_secret.resources[0].data['typha.crt'] | b64decode | trim | to_json }}"
    debug:
      msg: "The local Typha certificate and the remote one do equal!"
    when: typha_crt_local_content == typha_crt_remote_content
    register: typha_crt_local_remote_equality

  - name: Fail if the local Typha crt and the remote one do not match
    fail:
      msg: "The local Typha and the one stored in the Secret do not equal!\nSomething is weird."
    when: not typha_crt_local_remote_equality

# Generate and upload a new certificate if
# - first run (does not exist)
# - certificate expires soon
- name: Generate Typha certificate and create the Secret on k8s
  block:
  # The Typha private key is regenerated, if the Typha certificate is short to expire. This will
  # also lead to the recreation of the Typha certificate. The certificate is also recreated, if the
  # Typha CA certificate has changed.
  - name: Regenerate (force) private key of Typha certificate if crt is short to expire
    when: not (typha_crt_valid.valid_at.point_2  | default('false'))
    delegate_to: localhost
    openssl_privatekey:
      mode: 0600
      path: "{{ typha_key_path }}"
      size: 4096
      force: yes

  # TODO: we should protect this with a password and store it in a more secure place
  - name: Generate private key for Typha certificate
    delegate_to: localhost
    openssl_privatekey:
      mode: 0600
      path: "{{ typha_key_path }}"
      size: 4096

  # The certificate presents the Common Name (CN) as calico-typha. 
  # calico/node will be configured to verify this name.
  - name: Generate Certificate Signing Request for Typha certificate
    delegate_to: localhost
    openssl_csr:
      mode: 0664
      path: "{{ typha_csr_path }}"
      privatekey_path: "{{ typha_key_path }}"
      common_name: calico-typha
      subject:
        CN: calico-typha
      basic_constraints:
        - CA:TRUE

  - name: Sign the Certificate Signing Request of Typha
    delegate_to: localhost
    openssl_certificate:
      mode: 0664
      path: "{{ typha_crt_path }}"
      csr_path: "{{ typha_csr_path }}"
      ownca_path: "{{ typhaca_crt_path }}"
      ownca_privatekey_path: "{{ typhaca_key_path }}"
      entrust_not_after: "+365d"
      provider: ownca
    register: typha_crt_changed

  # The Secret should always be up-to-date
  - name: Apply Typha private key and certificate to Secret
    become: yes
    environment:
      KUBECONFIG: /etc/kubernetes/admin.conf
    k8s:
      apply: yes
      definition:
        apiVersion: v1
        data:
          typha.crt: "{{ lookup('file', typha_crt_path) | b64encode }}"
          typha.key: "{{ lookup('file', typha_key_path) | b64encode }}"
        kind: Secret
        metadata:
          name: calico-typha-certs
          namespace: kube-system
      validate:
        fail_on_error: yes
        strict: yes

# --------------------------------- #
# The following part deploys Typha. #
# --------------------------------- #

- name: Apply Typha resources
  become: yes
  environment:
    KUBECONFIG: /etc/kubernetes/admin.conf
  block:
  - name: Create ServiceAccount for calico-typha, configure RBAC, ClusterRole, ClusterRoleBinding
    k8s:
      definition: "{{ lookup('file', item) }}"
      apply: yes
      state: "present"
      validate:
        fail_on_error: yes
        strict: yes
    with_items:
    - typha-serviceaccount.yaml

  - name: Create Typha PodSecurityPolicy
    k8s:
      definition: "{{ lookup('file', item) }}"
      apply: yes
      state: "present"
      validate:
        fail_on_error: yes
        strict: yes
    with_items:
    - typha-psp.yaml

  - name: Deploy Typha Deployment
    k8s:
      definition: "{{ lookup('template', item) }}"
      apply: yes
      validate:
        fail_on_error: yes
        strict: yes
    with_items:
    - typha-deployment.yaml.j2

  - name: Deploy Typha service
    k8s:
      definition: "{{ lookup('file', item) }}"
      apply: yes
      validate:
        fail_on_error: yes
        strict: yes
      wait: yes
    with_items:
    - typha-service.yaml

- name: Restart Typha Pods on certificate_renewal
  become: yes
  environment:
    KUBECONFIG: /etc/kubernetes/admin.conf
  when: typha_crt_changed.changed
  block:
  - name: Restart Typha Pods
    command:
      argv:
        - kubectl
        - rollout
        - restart
        - deployment
        - calico-typha
        - -n
        - kube-system

  - name: Wait for Rollout Restart of Typha Pods to finish
    command:
      argv:
        - kubectl
        - rollout
        - status
        - deployment
        - calico-typha
        - -n
        - kube-system

# TODO: Validate that Typha is using TLS?
# https://docs.projectcalico.org/getting-started/kubernetes/hardway/install-typha#install-service
