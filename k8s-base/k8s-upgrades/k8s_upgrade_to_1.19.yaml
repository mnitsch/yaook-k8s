# This upgrade file is only for the k8s version 1.19.
# For now we keep for every minor update a separate upgrade file
# to avoid accidental major changes of old upgrade files.
#
# The upgrade v1.18->v1.19 differs from the previous ones as there
# is a dependency between kubelet and kubeadm and kubelet needs to
# to be updated prior to kubeadm.

- name: Detect user
  hosts: k8s_nodes
  gather_facts: false
  # we need *all* hosts.
  any_errors_fatal: true
  vars_files:
    - ../vars/etc.yaml
  roles:
  - detect_user

- name: Run pre-flight checks on all nodes
  hosts: k8s_nodes
  gather_facts: true
  roles:
    - kubernetes-upgrade-info
  tags:
    - kubernetes-upgrade-preflight-checks

- name: Upgrade the first master node
  hosts: "{{ (groups['masters'] | sort)[0] }}"
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - kubeadm-first-master
  roles:
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
      # kubeadm v1.19 depends on kubelet >=v1.19
    - name: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
      vars:
        k8s_kubelet_disable_customizations: true
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-apply
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade the residual master nodes
  hosts: "{{ (groups['masters'] | sort)[1:] }}"
  gather_facts: false
  serial: 1
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - kubeadm-other-masters
  roles:
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
      # kubeadm v1.19 depends on kubelet >=v1.19
    - name: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
      vars:
        k8s_kubelet_disable_customizations: true
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade the worker nodes
  hosts: workers
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
  tags:
    - workers
  serial: 1
  roles:
    - name: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
      # kubeadm v1.19 depends on kubelet >=v1.19
    - name: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
      vars:
        k8s_kubelet_disable_customizations: true
    - name: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Final upgrade note
  gather_facts: false
  hosts: "{{ (groups['masters'] | sort)[0] }}"
  tasks:
  - name: Final upgrade note
    delegate_to: localhost
    debug:
      msg: |
        🥳 Congratulations! 🎉

          \o/  The upgrade to
           |      {{ next_k8s_version }}
          / \  is complete.

        You can now change the k8s_version variable to
        {{ next_k8s_version | to_json }}.
